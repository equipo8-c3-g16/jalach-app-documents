CREATE TABLE `company` (
  `rut` int PRIMARY KEY NOT NULL,
  `business_name` varchar(255) NOT NULL,
  `user` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `phone` varchar(16) NOT NULL,
  `opening_time` time,
  `closing_time` time
);

CREATE TABLE `role` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `business_rut` int NOT NULL,
  `role` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `hourly` float NOT NULL
);

CREATE TABLE `category` (
  `code` varchar(255) PRIMARY KEY NOT NULL,
  `name` varchar(255) NOT NULL,
  `business_rut` int NOT NULL
);

CREATE TABLE `product` (
  `code` int PRIMARY KEY NOT NULL,
  `business_rut` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `stock` int NOT NULL,
  `category_code` varchar(255) NOT NULL,
  `cost` float NOT NULL,
  `price` float NOT NULL,
  `amount` int NOT NULL
);

CREATE TABLE `employee` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `business_rut` int NOT NULL,
  `role_id` int NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `weekly_hours` varchar(255) NOT NULL
);

ALTER TABLE `product` ADD FOREIGN KEY (`business_rut`) REFERENCES `company` (`rut`);

ALTER TABLE `category` ADD FOREIGN KEY (`business_rut`) REFERENCES `company` (`rut`);

ALTER TABLE `role` ADD FOREIGN KEY (`business_rut`) REFERENCES `company` (`rut`);

ALTER TABLE `product` ADD FOREIGN KEY (`category_code`) REFERENCES `category` (`code`);

ALTER TABLE `employee` ADD FOREIGN KEY (`business_rut`) REFERENCES `company` (`rut`);

ALTER TABLE `employee` ADD FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);
